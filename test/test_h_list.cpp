/*
Copyright 2020 Kallkod Oy

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "heterogeneous_list.h"
#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <string>
#include "helpers.h"

using kallkod::heterogeneous_list;

TEST(HList, DefaultConstructor)
{
    heterogeneous_list uut;
    (void)uut;
}

struct S1
{
    long double d;
    char c[11];
    bool operator==(const S1& s) const
    {
        if (strncmp(c, s.c, sizeof(c)))
            return false;
        return abs(d - s.d) < 0.0001L;
    }
};

TEST(S1, Comparison_operator)
{
    S1 uut {123, "hello"};
    S1 uut1{124, "hello"};

    ASSERT_FALSE(uut == uut1);

    S1 uut2{123, "world"};
    ASSERT_FALSE(uut == uut2);
}

TEST(HList, Push_back)
{
    heterogeneous_list uut;
    uut.push_back(32767);
    uut.push_back(std::string("hello world"));
    uut.push_back("another literal");
    uut.push_back(S1{});
}

TEST(HList, Front)
{
    heterogeneous_list uut;
    uut.push_back(321);
    ASSERT_EQ(321, uut.front<int>());
    uut.pop_front();
    uut.push_back(std::string("hello world"));
    ASSERT_EQ(std::string("hello world"), uut.front<std::string>());
    uut.pop_front();

    S1 mem{12, "dsa"};
    uut.push_back(mem);
    ASSERT_EQ(mem, uut.front<S1>());
}

TEST(HList, Empty)
{
    heterogeneous_list uut;
    ASSERT_TRUE(uut.empty());
    uut.push_back(0);
    ASSERT_FALSE(uut.empty());
    uut.pop_front();
    ASSERT_TRUE(uut.empty());
}

TEST(HList, Pop_front)
{
    heterogeneous_list uut;
    uut.push_back(32767);
    uut.push_back(std::string("hello world"));
    uut.push_back("another literal");
    uut.push_back(S1{});
    uut.pop_front();
    uut.pop_front();
    uut.pop_front();
    uut.pop_front();
    ASSERT_TRUE(uut.empty());
}

TEST(HList, WrongTypeOfData)
{
    heterogeneous_list uut;
    uut.push_back(std::string("text"));
    ASSERT_ANY_THROW(uut.front<int>());
}

TEST(HList, FrontFromEmpty)
{
    heterogeneous_list uut;
    ASSERT_ANY_THROW(uut.front<int>());
}

TEST(HList, InitializerList)
{
    auto list_of_values = {1, 3, 5, 7};
    heterogeneous_list uut(list_of_values);
    for (auto &i : list_of_values)
    {
        ASSERT_EQ(uut.front<int>(), i);
        uut.pop_front();
    }
    ASSERT_TRUE(uut.empty());
}

TEST(HList, Size)
{
    heterogeneous_list uut;
    ASSERT_EQ(uut.size(), 0);
    uut.push_back(32767);
    uut.push_back(32767);
    uut.push_back(32767);
    ASSERT_EQ(uut.size(), 3);
    uut.pop_front();
    uut.pop_front();
    uut.pop_front();
    ASSERT_EQ(uut.size(), 0);
}

TEST(HList, Size_InitializerList)
{
    auto list_of_values = {1, 3, 5, 7};
    heterogeneous_list uut(list_of_values);
    ASSERT_EQ(uut.size(), list_of_values.size());
    uut.pop_front();
    uut.pop_front();
    uut.pop_front();
    uut.pop_front();
    ASSERT_EQ(uut.size(), 0);
    std::initializer_list<int> empty_list;
    heterogeneous_list uut2(empty_list);
    ASSERT_EQ(uut2.size(), 0);
}

TEST(HList, Destructor)
{
    counter cnt;
    EXPECT_CALL(cnt, plus_one()).Times(1);

    heterogeneous_list uut;
    checked_destroy cd{ &cnt };
    uut.push_back(cd);
    cd.ptr = nullptr;
}

TEST(HList, TestPopFront)
{
    counter cnt;
    EXPECT_CALL(cnt, plus_one()).Times(1);

    heterogeneous_list uut;
    checked_destroy cd{ &cnt };
    uut.push_back(cd);
    cd.ptr = nullptr;
    uut.pop_front();
    testing::Mock::VerifyAndClearExpectations(&cnt);
}

TEST(HList, Iterator_plus_minus)
{
    heterogeneous_list uut;
    uut.push_back(1);
    uut.push_back(3);
    heterogeneous_list::iterator it;
    it = uut.begin();
    ++it;
    ASSERT_EQ(it.get<int>(), 3);
    --it;
    ASSERT_EQ(it.get<int>(), 1);
    it++;
    ASSERT_EQ(it.get<int>(), 3);
}

TEST(HList, Iterator_equality)
{
    heterogeneous_list uut1;
    uut1.push_back(1141);
    heterogeneous_list::iterator it1;
    it1 = uut1.begin();
    auto it2 = uut1.end();
    ASSERT_NE(it1, it2);
    --it2;
    ASSERT_EQ(it1, it2);
}

TEST(HList, Iterator)
{
    heterogeneous_list uut{1, 3, 5, 7};
    heterogeneous_list::iterator it;
    heterogeneous_list uut1;

    for (it = uut.begin(); it != uut.end(); it++)
    {
        uut1.push_back(it.get<int>());
    }

    ASSERT_EQ(uut.front<int>(), uut1.front<int>());
    uut1.pop_front();
    ASSERT_EQ(3, uut1.front<int>());
    uut1.pop_front();
    ASSERT_EQ(5, uut1.front<int>());
    uut1.pop_front();
    ASSERT_EQ(7, uut1.front<int>());
    uut1.pop_front();
    ASSERT_TRUE(uut1.empty());
}

TEST(HList, IteratorIncrement)
{
    heterogeneous_list uut;
    uut.push_back(1);
    auto i1 = uut.begin();
    auto i2 = uut.begin();

    ASSERT_EQ(++i1, uut.end());
    ASSERT_EQ(i2++, uut.begin());

    ASSERT_EQ(i1, i2);
    ASSERT_EQ(i1, uut.end());
}

#pragma once

/*
Copyright 2020 Kallkod Oy

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <gmock/gmock.h>

struct payload {
    std::string s;
    payload(std::string p)
        : s{p}
    {}

    payload(payload&& p) = default;
    payload(const payload& p) = default;

    payload& operator=(const payload& p) = delete;
};

bool operator==(const payload& a, const payload& b)
{
    return a.s == b.s;
}

struct counter
{
    MOCK_METHOD0(plus_one, void());
};

struct checked_construction
{
    checked_construction(counter& c)
    {
        c.plus_one();
    }

    checked_construction(checked_construction&&) = delete;
};

struct checked_destroy
{
    counter* ptr;

    checked_destroy(const checked_destroy& orig) = default;

    checked_destroy(counter* p)
        : ptr(p)
    {}

    checked_destroy(checked_destroy&& orig) noexcept
        : ptr(orig.ptr)
    {
        orig.ptr = nullptr;
    }

    ~checked_destroy()
    {
        if (ptr)
        {
            ptr->plus_one();
        }
    }
};

struct accumulator
{
    int summa = 0;

    void operator()(int* p) { summa += *p; }
};

struct checked_copy
{
    checked_copy(counter& cnt)
        : c(cnt)
    {
    }

    checked_copy(const checked_copy& src)
      : c(src.c)
    {
        c.plus_one();
    }

    checked_copy(checked_copy&&) = delete;
    counter& c;
};

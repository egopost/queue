/*
Copyright 2020 Kallkod Oy

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <iostream>
#include <chrono>
#include <thread>
#include <atomic>
#include <condition_variable>
#include "queue.h"
#include "circular_buffer.h"

using namespace std;
using namespace kallkod;
typedef int Type;

template <class T, class Unused>
using BufferAdapter = circular_buffer<T>;
using Queue = kallkod::queue<Type, BufferAdapter>;
//using Queue = Kallkod::Queue<Type>;

void read_print(Queue &d, atomic<bool> &flag)
{  
    while (flag)
    {
        cout << "received value: " << d.receive() << endl;
        this_thread::sleep_for(chrono::milliseconds(10));
    }
}

void write_print(Type x, Queue &d, atomic<bool> &flag)
{
    while (flag)
    {
        d.send(++x);
    }
}

void print_last_value(Queue &d)
{
    cout << "Last vector value: " << d.get_last_value() << endl;
}

auto start()
{
    auto start = chrono::high_resolution_clock::now();
    return start; 
}

auto stop()
{
    auto stop = chrono::high_resolution_clock::now();
    return stop; 
}

template <typename T>
void duration(T start, T stop)
{
    unsigned int time_interval = chrono::duration_cast<chrono::milliseconds>(stop - start).count(); 
    cout << "Time calculaton (vector): " << time_interval << " milliseconds" << endl;
}

int main(int argc, char ** argv)
{
    int number; // Number of iteration
    atomic <bool> flag;
    flag = true;
    atomic <bool> flag_reader;
    flag_reader = true;
    Type lim{int(1e7)};
    Type fill_level{int(8e6)};

    if (argc < 2)
    {
        cout << "please provide 1 argument." << endl;
        return -1;
    }
    else
    {
        number = atoi(argv[1]);
    }
    
    Queue D(lim);
    
    int m = 77;
    auto a = [&]()
    {
	return ++m;
    };
    
    D.fill(a, fill_level);
    
    print_last_value(D);
    
    auto start_t = start();
      
    for (int i = 0; i < number; ++i)
    {
        thread reader(&read_print, ref(D), ref(flag_reader));

        this_thread::sleep_for(chrono::milliseconds(300));

        thread sender1(&write_print, 1, ref(D), ref(flag));
        thread sender2(&write_print, 100, ref(D), ref(flag));

        this_thread::sleep_for(chrono::seconds(5));
        flag = 0;

        sender1.join();
        cout << "sender1 done." << endl;
        sender2.join();
        cout << "sender2 done." << endl;

        flag_reader = 0;
        D.try_send(-1);
        reader.join();
        cout << "reader done." << endl;
    }
    
    auto stop_t = stop();
    
    duration(start_t, stop_t);
    
    return 0;
}
